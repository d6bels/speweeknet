#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "lib/xtcp.h"
#include "lib/xio.h"
#include "lib/connection.h"

#define SERVER_PORT 1500
#define MAX_MSG 1020

int main (int argc, char * argv[]) {

	int sockfd;
	socklen_t addrLen;
	struct sockaddr_in clientAddr, servAddr;
	char msgbuf[MAX_MSG];

	// Command-line error check
	if (argc < 3) {
		puts("Invalid arguments\nUsage: ./client ip_address message\n");
		return 1;
	}

	printf("Connection to %s\n", argv[1]);  // argv[1] : IP Address, 2: msg
	// Creating UDP socket
	if((sockfd = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror(argv[0]);
		return 1;
	}

	// Binding socket/address-port
	clientAddr.sin_family = AF_INET;
	clientAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	clientAddr.sin_port = htons(0);
	if(bind(sockfd, (struct sockaddr *)&clientAddr, sizeof clientAddr) == -1) {
		perror("bind error");
		return 1;
	}

	// Filling server address structure
	servAddr.sin_family = AF_INET;
	// argv[1] : IP Address
	if(inet_aton(argv[1], &(servAddr.sin_addr)) == 0) {
		printf("Invalid IP address format <%s>\n", argv[1]);
		return 1;
	}
	servAddr.sin_port = htons(SERVER_PORT);

	// Connection to the server
	addrLen = (socklen_t) sizeof(servAddr);
	if(xconnect(sockfd, (struct sockaddr *)&servAddr, addrLen) == -1) {
		perror("Connection failed");
		return 1;
	}

	else {
		printf("Connection succeed ! (%s:%d)\n", inet_ntoa(servAddr.sin_addr), ntohs(servAddr.sin_port));

		// Send 3 messages
		for(int i=1; i<=3; i++) {

			// Récup du message
			//strcpy(msgbuf, argv[2]);
			sprintf(msgbuf, "%s/%d", argv[2], i);

			if(xwrite(sockfd, msgbuf, strlen(msgbuf)+1) == -1 ) {
				perror("Write");
				return -1;
			}
			else {
				printf("Sent %s\n", msgbuf);
			}
		}

		puts("Closing...");
		xclose(sockfd);
		puts("Done.");
	}

	close(sockfd);
	return 0;
}
