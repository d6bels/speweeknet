#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "lib/xio.h"
#include "lib/connection.h"

#define SERVER_PORT 1500
#define MAX_MSG 1020

int main (void) {

	int sockfd, cnxSock;
	socklen_t addrLen;
	struct sockaddr_in clientAddr, serverAddr;
	char msgbuf[MAX_MSG];

	// Creating socket
	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket creation");
		return 1;
	}

	// Binding it
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(SERVER_PORT);
	if (bind (sockfd, (struct sockaddr *)&serverAddr, sizeof serverAddr) == -1) {
		perror("bind error");
		return 1;
	}

	// Listen (optionnel)
	if(xlisten(sockfd, 5) == -1) {
		perror("listen error");
		return 1;
	}

	while(1) {
		addrLen = sizeof(clientAddr);
		// Accept connection
		puts("Waiting for incoming connections...");
		if((cnxSock = xaccept(sockfd, (struct sockaddr *)&clientAddr, &addrLen)) == -1) {
			perror("Accept connection failed");
			return 1;
		}
		else {
			printf("Connection accepted ! (%s:%d)\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));

			for(;;) {
				int retv = xread(cnxSock, msgbuf, sizeof(msgbuf)+1);
				// Read fails
				if(retv == -1) {
					perror("Fail");
				}
				// Connection was closed, wait for other messages
				else if(connectionIsClosed()) {
					puts("Done.");
					break;
				}
				// Received message
				else {
					printf("Received : %s\n", msgbuf);
				}
			}
		}
	}
	return 0;
}
