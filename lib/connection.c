/*
 * connection.c
 * authors: Bleniat, Faure, Gaillard, Lop
 * Définit les fonctions spécifiques à la connexion
 */

#include "connection.h"

// Hashmap for opened connections
Connection_t currentConnection;

// Connexion
int xaccept(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {

	TcpPacket_t *receivePacket, *sendPacket, idealPacket;
	int cnxsd;
	struct sockaddr_in *sockAddr;

	// Set receivePacket memory
	receivePacket = malloc (sizeof(TcpPacket_t));

	// ---- LECTURE SYN ---- //
	if (recvfrom(sockfd, receivePacket, sizeof(TcpPacket_t), 0, addr, addrlen) == -1) {
		perror("=> xaccept failed to receive SYN message");
		return -1;
	}

	// Verification des données reçues
	initPacket(&idealPacket);
	idealPacket.syn = 1 ;
	if (xcheck(idealPacket, *receivePacket) == -1) {
		errno = EINVAL;
		perror("=> xaccept wrong received SYN packet");
		return -1;
	}

	// Initialisation de la structure xconnection
	currentConnection.state = SYNRECEIVED;
	sockAddr = (struct sockaddr_in *) addr;
	currentConnection.addr = *sockAddr;
	currentConnection.addrlen = *addrlen;
	currentConnection.seqNum = 0;
	currentConnection.ackNum = receivePacket->seqNum + 1;
	
	// ---- SOCKET SYSTEM ---- //

	// création du socket spécifique à ce client
	cnxsd = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in newSocket;
	newSocket.sin_family = AF_INET;
	newSocket.sin_addr.s_addr = htonl(INADDR_ANY);
	newSocket.sin_port = htons(1501);
	if(bind(cnxsd, (struct sockaddr*)&newSocket, sizeof(newSocket))) {
		perror("=> xaccept failed binding new socket");
	}

	// ---- ECRITURE SYN+ACK ---- //
	sendPacket = malloc(sizeof(TcpPacket_t));
	initPacket(sendPacket);
	sendPacket->srcPort = newSocket.sin_port ;
	sendPacket->dstPort = receivePacket->srcPort ;
	sendPacket->seqNum = currentConnection.seqNum ;
	sendPacket->ackNum = currentConnection.ackNum ;
	sendPacket->syn = 1 ;
	sendPacket->ack = 1 ;

	if (sendto(cnxsd, sendPacket, sizeof(TcpPacket_t), 0, addr, *addrlen)== -1) {
		perror("=> xaccept failed to send SYN+ACK message");
		return -1;
	}

	// ---- LECTURE ACK ---- //
	// Préparation du packet idéal
	initPacket(&idealPacket);
	idealPacket.srcPort = receivePacket->srcPort ;
	idealPacket.dstPort = newSocket.sin_port ;
	idealPacket.ackNum = receivePacket->seqNum +1 ;
	idealPacket.ack = 1 ;

	if (recvfrom(cnxsd, receivePacket, sizeof(TcpPacket_t), 0, addr, addrlen) == -1) {
		perror("=> xaccept failed to receive ACK message");
		return -1;
	}

	// Vérification 
	if (xcheck(idealPacket, *receivePacket) == -1) {
		errno = EINVAL;
		perror("=> xaccept wrong received ACK packet");
		return -1;
	}

	// Edition de la structure xconnection
	currentConnection.state = ESTABLISHED;
	currentConnection.seqNum = receivePacket->ackNum + 1;
	currentConnection.ackNum = receivePacket->seqNum + 1;


	free(receivePacket);
	free(sendPacket);
	return cnxsd;
}

int xconnect(int sockfd, struct sockaddr* addr, socklen_t addrlen) {

	TcpPacket_t *receivePacket, *sendPacket, idealPacket;
	struct sockaddr_in *paramAddr, *clientSockAddrIn, *sockAddr;
	struct sockaddr clientSockAddr;
	socklen_t clientSockAddrLen;
	getsockname(sockfd, &clientSockAddr, &clientSockAddrLen);
	clientSockAddrIn = (struct sockaddr_in *) &clientSockAddr;
	
	// Set receivePacket memory
	receivePacket = malloc (sizeof(TcpPacket_t));

	// Initialisation de la structure xconnection
	currentConnection.state = CLOSED;
	currentConnection.seqNum = 0;
	currentConnection.ackNum = 0;


	// ---- ECRITURE SYN ---- //
	// Préparation de la trame TCP
	paramAddr = (struct sockaddr_in *) addr;
	sendPacket = malloc(sizeof(TcpPacket_t));
	initPacket(sendPacket);
	sendPacket->srcPort = clientSockAddrIn->sin_port ;
	sendPacket->dstPort = paramAddr->sin_port ;
	sendPacket->syn = 1 ;

	// Envoi de SYN
	if (sendto(sockfd, sendPacket, sizeof(TcpPacket_t), 0, addr, addrlen) == -1) {
		perror("=> xconnect failed to send SYN message");
		return -1;
	}

	currentConnection.state = SYNSENT;

	// ---- LECTURE SYN+ACK ---- //
	// Préparation du packet idéal
	initPacket(&idealPacket);
	idealPacket.ackNum = sendPacket->seqNum +1 ;
	idealPacket.syn = 1 ;
	idealPacket.ack = 1 ;

	// Réception du SYN+ACK
	if (recvfrom(sockfd, (TcpPacket_t *) receivePacket, sizeof(TcpPacket_t), 0, addr, &addrlen) == -1) {
		perror("=> xconnect failed to receive SYN+ACK from the server");
		return -1;
	}

	// Vérification 
	if (xcheck(idealPacket, *receivePacket) == -1) {
		perror("=> xconnect wrong received SYN+ACK packet");
		return -1;
	}

	// Initialisation de la structure xconnection
	sockAddr = (struct sockaddr_in *) addr;
	currentConnection.addr = *sockAddr;
	currentConnection.addrlen = addrlen;
	currentConnection.seqNum = receivePacket->ackNum + 1;
	currentConnection.ackNum = receivePacket->seqNum + 1;
	
	// Mise à jour du port de connexion


	// ---- ECRITURE ACK ---- //
	// Préparation du ACK
	sendPacket = malloc(sizeof(TcpPacket_t));
	initPacket(sendPacket);
	sendPacket->srcPort = receivePacket->dstPort ;
	sendPacket->dstPort = receivePacket->srcPort ;
	sendPacket->ackNum = currentConnection.ackNum ;
	sendPacket->ack = 1 ;

	// Envoi du ACK
	if (sendto(sockfd, sendPacket, sizeof(TcpPacket_t), 0, addr, addrlen) == -1) {
		perror("=> xconnect failed to send ACK message");
		return -1;
	}

	currentConnection.state = ESTABLISHED;

	free(receivePacket);
	free(sendPacket);
	return 0;
}

int xlisten(int sockfd, int backlog) {

	// TODO Ghetto warning suppress
	sockfd = 0;
	backlog = 0;

	// TODO créer les informations de connexions en attente

	return 0;
}

// Fermeture de connexion (lancé par le client)
int xclose(int sockfd) {

	TcpPacket_t *receivePacket, *sendPacket, idealPacket;
	socklen_t addrlen = sizeof(currentConnection.addr);

	// Set receivePacket memory
	receivePacket = malloc (sizeof(TcpPacket_t));

	// ---- ECRITURE FIN ---- //
	// Préparation de la trame TCP
	sendPacket = malloc(sizeof(TcpPacket_t));
	initPacket(sendPacket);
	sendPacket->dstPort = currentConnection.addr.sin_port ;
	sendPacket->fin = 1 ;
	sendPacket->seqNum = currentConnection.seqNum;
	sendPacket->ackNum = currentConnection.ackNum;

	// Envoi de FIN
	if (sendto(sockfd, sendPacket, sizeof(TcpPacket_t), 0, (struct sockaddr*)&(currentConnection.addr), addrlen) == -1) {
		perror("=> xclose failed to send FIN message");
		return -1;
	}

	currentConnection.state = FIN_WAIT1;

	// ---- LECTURE FIN+ACK ---- //
	// Préparation du packet idéal
	initPacket(&idealPacket);
	idealPacket.ackNum = sendPacket->seqNum +1 ;
	idealPacket.seqNum = currentConnection.seqNum;
	idealPacket.fin = 1 ;
	idealPacket.ack = 1 ;

	// Réception du FIN+ACK
	if (recvfrom(sockfd, (TcpPacket_t *) receivePacket, sizeof(TcpPacket_t), 0, (struct sockaddr*)&(currentConnection.addr), &addrlen) == -1) {
		perror("=> xclose failed to receive FIN+ACK from the server");
		return -1;
	}

	// Vérification 
	if (xcheck(idealPacket, *receivePacket) == -1) {
		perror("=> xclose wrong received FIN+ACK packet");
		return -1;
	}

	// Initialisation de la structure xconnection
	currentConnection.seqNum = receivePacket->ackNum + 1;
	currentConnection.ackNum = receivePacket->seqNum + 1;
	
	// Mise à jour du port de connexion


	// ---- ECRITURE ACK ---- //
	// Préparation du ACK
	sendPacket = malloc(sizeof(TcpPacket_t));
	initPacket(sendPacket);
	sendPacket->srcPort = receivePacket->dstPort ;
	sendPacket->dstPort = receivePacket->srcPort ;
	sendPacket->ackNum = currentConnection.ackNum ;
	sendPacket->ack = 1 ;

	// Envoi du ACK
	if (sendto(sockfd, sendPacket, sizeof(TcpPacket_t), 0, (struct sockaddr*)&(currentConnection.addr), addrlen) == -1) {
		perror("=> xclose failed to send ACK message");
		return -1;
	}

	currentConnection.state = CLOSING;

	// FERMETURE DU SOCKET
	if (!close(sockfd)) {
		currentConnection.state = CLOSED;
		free(receivePacket);
		free(sendPacket);
		return 0;
	}

	perror("=> xclose failure");
	return -1;
}

int xclose_accept(int sockfd, TcpPacket_t *receivePacket) {
	
	TcpPacket_t sendPacket, idealPacket;
	socklen_t addrlen = sizeof(currentConnection.addr);

	// Initialisation de la structure xconnection
	currentConnection.state = CLOSE_WAIT;
	currentConnection.ackNum = receivePacket->seqNum + 1;

	// ---- ECRITURE SYN+ACK ---- //
	initPacket(&sendPacket);
	sendPacket.srcPort = receivePacket->dstPort ;
	sendPacket.dstPort = receivePacket->srcPort ;
	sendPacket.seqNum = currentConnection.seqNum ;
	sendPacket.ackNum = currentConnection.ackNum ;
	sendPacket.fin = 1 ;
	sendPacket.ack = 1 ;

	if (sendto(sockfd, &sendPacket, sizeof(TcpPacket_t), 0, (struct sockaddr*)&(currentConnection.addr), addrlen)== -1) {
		perror("=> xclose_accept failed to send FIN+ACK message");
		return -1;
	}

	// ---- LECTURE ACK ---- //
	// Préparation du packet idéal
	initPacket(&idealPacket);
	idealPacket.srcPort = receivePacket->srcPort ;
	idealPacket.dstPort = receivePacket->dstPort ;
	idealPacket.ackNum = receivePacket->seqNum +1 ;
	idealPacket.ack = 1 ;

	if (recvfrom(sockfd, receivePacket, sizeof(TcpPacket_t), 0, (struct sockaddr*)&(currentConnection.addr), &addrlen) == -1) {
		perror("=> xclose_accept failed to receive ACK message");
		return -1;
	}

	// Vérification 
	if (xcheck(idealPacket, *receivePacket) == -1) {
		errno = EINVAL;
		perror("=> xclose_accept wrong received ACK packet");
		return -1;
	}

	// Edition de la structure xconnection
	currentConnection.state = LAST_ACK;
	currentConnection.seqNum = receivePacket->ackNum + 1;
	currentConnection.ackNum = receivePacket->seqNum + 1;


	

	// FERMETURE DU SOCKET
	if (!close(sockfd)) {
		currentConnection.state = CLOSED;
		return 0;
	}

	puts("=> xclose_accept failure");
	return -1;
}


// UTILS
int xcheck(const TcpPacket_t idealPacket, const TcpPacket_t aPacket) {
	if ( (idealPacket.srcPort == aPacket.srcPort || idealPacket.srcPort == 0)
		&& ( idealPacket.dstPort == aPacket.dstPort || idealPacket.dstPort == 0)
		&& (idealPacket.seqNum == aPacket.seqNum || idealPacket.seqNum == 0)
		&& (idealPacket.ackNum == aPacket.ackNum || idealPacket.ackNum == 0)
		&& idealPacket.window == aPacket.window
		&& idealPacket.crc == aPacket.crc
		&& idealPacket.syn == aPacket.syn
		&& idealPacket.fin == aPacket.fin
		&& idealPacket.ack == aPacket.ack
		&& idealPacket.rst == aPacket.rst ) {
		return 0;
	}
	return -1;
}
