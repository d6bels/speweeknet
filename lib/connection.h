/*
 * connection.h
 * authors: Bleniat, Faure, Gaillard, Lop
 * Définit les fonctions spécifiques à la connexion
 */

#ifndef INC_CONNECTION
#define INC_CONNECTION

#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "xtcp.h"

// pendding connection info type
typedef struct {
	unsigned maxPendingClient;
	unsigned nbPendingClient;
	unsigned cursor;
	TcpPacket_t *pendingClientTab; // à allouer en fonction du listen
} PendingConnectionInfo;

// Connexion
int xaccept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);

int xconnect(int sockfd, struct sockaddr* addr, socklen_t addrlen);

int xlisten(int sockfd, int backlog);

// Fermeture de connexion
int xclose(int sockfd);

int xclose_accept(int sockfd, TcpPacket_t *receivePacket);

//UTILS
int xcheck(const TcpPacket_t idealPacket, const TcpPacket_t aPacket);

#endif
