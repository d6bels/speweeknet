/*
 * exchange.h
 * authors: Bleniat, Faure, Gaillard, Lop
 * Définit les fonctions spécifiques à l'échange de messages tcp
 */

#ifndef INC_XIO
#define INC_XIO

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include "xtcp.h"
#include "connection.h"

// Envoi d'un buffer buf de taille bufsz dans la socket sockfd
ssize_t xwrite(int sockd, const void *buf, size_t bufsz);

// Réception d'un message dans le buffer buf de taille bufsz
ssize_t xread(int sockd, void *buf, size_t bufsz);

#endif
