#include "xtcp.h"

void initPacket(TcpPacket_t *p) {
	memset(p, 0, sizeof(TcpPacket_t));
}

int setPacketData(TcpPacket_t *p, const void *buf, const size_t len) {

	// TODO : Fragmenter le packet si la taille est trop grande, mais là on rêve :-)
	if(len > sizeof(p->data)) {
		errno = EIO;
		perror("Cannot fragment packet");
		return -1 ;
	}

	// Copie du buffer
	(void)memcpy(&(p->data), buf, len);

	return 0;
}

int connectionIsClosed() {
	return (currentConnection.state==CLOSED ? 1 : 0);
}
