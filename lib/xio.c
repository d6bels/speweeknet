/*
 * exchange.c
 * authors: Bleniat, Faure, Gaillard, Lop
 * Définit les fonctions spécifiques à l'échange de messages tcp
 */

#include "xio.h"

// Envoi de données
ssize_t xwrite(int sockd, const void *buf, size_t bufsz) {

	// Création du packet
	TcpPacket_t packet;
	initPacket(&packet);

	// Ajout data
	if(setPacketData(&packet, buf, bufsz) != 0) {
		return -1;
	}

	packet.dstPort = currentConnection.addr.sin_port;
	packet.seqNum = currentConnection.ackNum;
	packet.ackNum = currentConnection.seqNum + bufsz;

	if(sendto(sockd, &packet, sizeof(packet), 0, (struct sockaddr*)&(currentConnection.addr), (socklen_t)sizeof(currentConnection.addr)) == -1) {
		perror("Sending problem");
		return -1;
	}

	// Attente du ACK
	// TODO : Timeout !
	TcpPacket_t recvPacket;
	initPacket(&recvPacket);
	socklen_t addrlen = sizeof(currentConnection.addr);
	size_t recvsz = 0;
	char recvbuf[2000];

	recvsz = recvfrom(sockd, recvbuf, sizeof(TcpPacket_t), 0, (struct sockaddr*)&(currentConnection.addr), &addrlen);
	if((int)recvsz == -1) {
		perror("Receiving ack problem");
		return -1;
	}

	memcpy(&recvPacket, recvbuf, recvsz);

	// Si pas un ACK, on renvoi le packet
	if(!recvPacket.ack) {
		xwrite(sockd, buf, bufsz);
	}

	return bufsz;
}

// Réception de données
ssize_t xread(int sockd, void *buf, size_t bufsz) {

	TcpPacket_t recvPacket;
	initPacket(&recvPacket);
	socklen_t addrlen = sizeof(currentConnection.addr);
	size_t recvsz = 0;

	// Récup du packet
	recvsz = recvfrom(sockd, buf, bufsz, 0, (struct sockaddr*)&(currentConnection.addr), &addrlen);
	if((int)recvsz == -1) {
		perror("Receiving problem");
		return -1;
	}

	// Récupération du packet TCP
	// TODO : Flags à 0 ?
	memcpy(&recvPacket, buf, recvsz);

	// Maj connexion courante
	currentConnection.seqNum = recvPacket.seqNum;
	currentConnection.ackNum = recvPacket.ackNum;

	// Retour de la data
	memcpy(buf, &recvPacket.data, DATA_SIZE);

	// Test si fin connexion
	if(recvPacket.fin==1) {
		puts("Closing...");

		if(xclose_accept(sockd, &recvPacket)==-1) {
			return -1;
		}

		return sizeof(buf);
	}

	else {
		// Création du packet ack
		TcpPacket_t ackPacket;
		initPacket(&ackPacket);
		ackPacket.ack = 1;
		ackPacket.seqNum = recvPacket.ackNum;
		ackPacket.ackNum = recvPacket.seqNum + sizeof(recvPacket.data);

		// Sending ack
		if(sendto(sockd, &ackPacket, sizeof(ackPacket), 0, (struct sockaddr*)&(currentConnection.addr), addrlen) == -1) {
			perror("Sending ACK problem");
			return -1;
		}

		return sizeof(buf);
	}
}

