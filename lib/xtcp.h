/*
 * def.h
 * authors: Bleniat, Faure, Gaillard, Lop
 * Définit les énumérations, structures et types
 */

#ifndef INC_XTCP
#define INC_XTCP

#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>

typedef enum  {
	CLOSED,
	SYNSENT,
	ESTABLISHED,
	SYNRECEIVED,
	LISTEN,
	FIN_WAIT1,
	FIN_WAIT2,
	CLOSING,
	TIME_WAIT,
	CLOSE_WAIT,
	LAST_ACK
} TcpState_t;


#define DATA_SIZE (1000)

typedef struct	{
	unsigned short srcPort;
	unsigned short dstPort;
	unsigned int seqNum;
	unsigned int ackNum;
	unsigned short window;
	unsigned short crc;
	char syn;
	char fin;
	char ack;
	char rst;
	char data[DATA_SIZE];
} TcpPacket_t;

typedef struct {
	TcpState_t state;
	char window[1000];
	struct sockaddr_in addr;
	socklen_t addrlen;
	int seqNum;
	int ackNum;
	//... TODO autres informations
} Connection_t;


// Utilise pour se souvenir des infos de connexion
// TODO : Utiliser une hashmap, mais impossible de la faire marcher...
extern Connection_t currentConnection;

// Functions

// Init d'un packet avec 0 partout
void initPacket(TcpPacket_t *p);

// Ajoute la data
// Return 0 OK, -1 sinon
int setPacketData(TcpPacket_t *p, const void *buf, const size_t len);

// Tells if connection is closed
int connectionIsClosed();

#endif
