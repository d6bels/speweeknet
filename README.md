TCP Light
======
Special network programming week !

Implementing a light TCP protocol over UDP.

Team
----

* Adrien FAURE
* Clémence LOP
* Jorys GAILLARD
* Kevin BLENIAT

Directory
-----------

* */* : Contains client and server code and this readme
 * *lib* : Contains librairies and utils
 * *doc* : Project documentation

Build
------
Default *Makefile* rule builds both *client* and *server*.  
To build them separately use the corresponding rules :  

* `make client` : Builds the *client* binary
* `make server` : Builds the *server* binary

Other rules :

* `make clean` : Cleans all *.o* an binary files from *project's root* and *lib* directories.

Makefile is silent by default.  
You can turn on verbose mode by appending `Q=` to your makefile rule (i.e. `make server Q=`)

### Libs

*Previous version*
Using the hashmap provided by [https://github.com/petewarden/c_hashmap](https://github.com/petewarden/c_hashma

Use
-----

Functions `xwrite` & `xread` shall not be used prior getting the connection set up.  
The connection created using the classic functions `xlisten`, `xconnect`, `xaccept` and closed with `xclose`.

Test
-----

To test the library, you can use the client and server binaries.

* `./server` : Run the server which waits for connections on all IPs, port `SERVER_PORT`, and just prints the received messages.
* `./client IP message` : Runs the client sends three times the given message to IP:`SERVER_PORT` before closing the connection.