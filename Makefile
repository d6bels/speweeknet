CC = gcc
CFLAGS = -W -Wall -g
LDFLAGS =
 
# Objs only contain lib parts, useful to have two programs to build that use the same lib
LIBDIR = lib/
SRC = $(wildcard $(LIBDIR)*.c)
OBJS = $(SRC:.c=.o)

# Our binaries
BIN = client server

# Quiet mode
Q = @
 
# Builds both client and server
all : $(BIN)

client : client.o $(OBJS)
	$(Q)$(CC) $(LDFLAGS) -o $@ $^
	
server : server.o $(OBJS)
	$(Q)$(CC) $(LDFLAGS) -o $@ $^
   
# Default rule to build .o
%.o : %.c
	$(Q)$(CC) $(CFLAGS) -o $@ -c $<
  
clean :
	$(Q)rm -f *.o
	$(Q)rm -f $(LIBDIR)*.o
	$(Q)rm -f $(BIN)
	
again:
	clear
	make clean
	make
	